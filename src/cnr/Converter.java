package cnr;

public class Converter {

    public String Convert(String liczba, int a, int b) {
       for (char ch : liczba.toCharArray()) {
            int bound;
            if (a == 16) bound = 9;
            else bound = a - 1;

            if (!Character.isDigit(ch) && !((int)ch >= 65 && (int)ch <= 70) && !((int)ch >= 97 && (int)ch <= 102)) {
                throw new IllegalArgumentException("Podany string zawiera znak,"
                        + " który nie jest cyfra (" + ch + ")\n");
            }
            
            int c = Character.getNumericValue(ch);
            if (c > bound && a != 16)
                throw new IllegalArgumentException("Niepoprawne dane.");
        }
        
        if (a != 2 && a != 8 && a != 10 && a != 16) {
            throw new IllegalArgumentException("Podstawa a = " + a + " nie jest obsługiwana\n");
        }
        if (b != 2 && b != 8 && b != 10 && b != 16) {
            throw new IllegalArgumentException("Podstawa b = " + b + " nie jest obsługiwana\n");
        }

        String result = liczba;
        
        if (b == 10 && !liczba.isEmpty()) {
            result = Integer.toString(Integer.parseInt(liczba, a), b);
        }

        return result;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cnr;

import junit.framework.Assert;
import org.junit.Test;

/**
 *
 * @author N7 Gamer
 */
public class ConverterTest {
    
    @Test
    public void whenTheNumberInStringIsAnIntegerThenNoExceptionIsThrown() {
        String number  = "1231";
        int a = 10;
        int b = 10;
        new Converter().Convert(number, a, b);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void whenTheStringContainsCharactersOtherThanDigitsThenExceptionIsThrown() {
        String number = "10.4";
        int a = 10;
        int b = 10;
        new Converter().Convert(number, a, b);
    }

    @Test
    public void whenProperNumericalBasisAreUsedThenNoExceptionIsThrown() {
        String liczba = "";
        int a = 2;
        int b = 10;
        Converter instance = new Converter();
        instance.Convert(liczba, a, b);
        Assert.assertTrue(true);
    }

    @Test(expected = IllegalArgumentException.class)
    public void whenImproperNumericalBasisAreUsedThenExceptionIsThrown() {
        String liczba = "";
        int a = -1;
        int b = 18;
        Converter instance = new Converter();
        instance.Convert(liczba, a, b);
    }
    
    @Test
    public void whenTargetBaseIs10ThenTheNumberIsConverted() {
        String liczba = "1010";
        int a = 2;
        int b = 10;
        String expected = "10";
        String result = new Converter().Convert(liczba, a, b);
        Assert.assertEquals(expected, result);
    }
    
    @Test
    public void whenTargetBaseIsNot10ThenTheNumberIsNotConverted() {
        String liczba = "1010";
        int a = 2;
        int b = 8;
        String expected = "1010";
        String result = new Converter().Convert(liczba, a, b);
        Assert.assertEquals(expected, result);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void whenUnsupportedNumericalBasisAreUsedThenExceptionIsThrown() {
        String liczba = "";
        int a = 2;
        int b = 3;
        Converter instance = new Converter();
        instance.Convert(liczba, a, b);
    }
    
    @Test
    public void whenSupportedNumericalBasisAreUsedThenNoExceptionIsThrown() {
        String liczba = "";
        int a = 8;
        int b = 16;
        Converter instance = new Converter();
        instance.Convert(liczba, a, b);
        Assert.assertTrue(true);
    }
    
    @Test
    public void whenStringContainsNumbersAndLettersABCDEFThenNoExceptionIsThrown() {
        String liczba = "12346ABCDEF";
        int a = 16;
        int b = 16;
        Converter instance = new Converter();
        instance.Convert(liczba, a, b);
    }
    
    @Test
    public void whenStringContainsNumbersAndLettersabcdefThenNoExceptionIsThrown() {
        String liczba = "12346abcdef";
        int a = 16;
        int b = 16;
        Converter instance = new Converter();
        instance.Convert(liczba, a, b);
    }

    @Test(expected = IllegalArgumentException.class)
    public void whenStringContainsInvalidCharacterForNumericalBasisThenExceptionIsThrown() {
        String liczba = "01010201";
        int a = 2;
        int b = 16;
        Converter instance = new Converter();
        instance.Convert(liczba, a, b);
    }
    
    @Test
    public void methodConvertsStringToDifferentNumericalBasis() {
        String liczba = "1010";
        int a = 2;
        int b = 10;
        Converter instance = new Converter();
        String wynik = instance.Convert(liczba, a, b);
        Assert.assertTrue(wynik.equalsIgnoreCase("10"));
    }

}